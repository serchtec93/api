from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
import requests
import json
from django.conf import settings


# Create your views here.
class Jokes(generics.RetrieveAPIView):
    serializer_class = None
    
    def get(self, request, *args, **kwargs):
        item = []
        value = list()
        while len(item)<25:
            response = requests.get(URL=settings.URL)
            if response.status_code not in (200, 201):
                return Response({"data": "NULL"}, status=status.HTTP_404_NOT_FOUND)
            data = response.json()
            if data['id'] not in [x["id"] for x in item]:
                item.append(data)
        for i in range(25):
            value.append(item[i])
        return Response({"data":value}, status=status.HTTP_200_OK)

